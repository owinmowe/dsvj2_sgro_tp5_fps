﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour, IDamageable
{
    [Header("Gameplay Related")]
    [SerializeField] int startingLife = 100;
    int currentLife;
    int currentPoints;

    [Header("GUI Related")]
    [SerializeField] TextMeshProUGUI lifeText;
    [SerializeField] TextMeshProUGUI ammoText;
    [SerializeField] TextMeshProUGUI clipsText;
    [SerializeField] TextMeshProUGUI scoreText;
    string lifeStartingText;
    string ammoStartingText;
    string clipsStartingText;
    string scoreStartingText;

    [Header("Gun Related")]
    [SerializeField] Animator gunAnimator;
    [SerializeField] AudioSource gunAudioSource;
    [SerializeField] string shootAnimationTriggerName = "Shoot";
    [SerializeField] string reloadAnimationTriggerName = "Reload";
    [SerializeField] AudioClip shootAudio;
    [SerializeField] AudioClip reloadAudio;
    [SerializeField] AudioClip hitAudio;
    [SerializeField] int maxAmmoClips = 3;
    [SerializeField] int maxAmmoPerClip = 10;
    [SerializeField] int gunDamage = 50;
    [SerializeField] float reloadSpeed = 1;
    [SerializeField] float shootingSpeed = .1f;
    [SerializeField] float weaponRange = 20;
    int currentAmmo;
    int currentClips;
    bool inAction;

    // Start is called before the first frame update

    void Start()
    {
        Init();
    }

    private void Init()
    {
        GameManager.Get().SetCurrentPlayerReference(this);
        InitGUI();
    }

    private void InitGUI()
    {
        currentLife = startingLife;
        currentAmmo = maxAmmoPerClip;
        currentClips = maxAmmoClips;
        lifeStartingText = lifeText.text;
        RefreshLifeText();
        ammoStartingText = ammoText.text;
        RefreshAmmoText();
        scoreStartingText = scoreText.text;
        RefreshPointsText();
        clipsStartingText = clipsText.text;
        RefreshClipsText();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }
    }

    private void Shoot()
    {
        if(currentAmmo > 0 && !inAction)
        {
            RaycastHit hit;
            Vector3 mouseInput = Input.mousePosition;
            var Ray = Camera.main.ScreenPointToRay(mouseInput);
            if (Physics.Raycast(Ray, out hit, weaponRange))
            {
                if(hit.collider.gameObject.GetComponent<IDamageable>() != null)
                {
                    gunAudioSource.PlayOneShot(hitAudio);
                    hit.collider.gameObject.GetComponent<IDamageable>().TakeDamage(gunDamage);
                }
            }
            inAction = true;
            currentAmmo--;
            RefreshAmmoText();
            gunAudioSource.PlayOneShot(shootAudio);
            StartCoroutine(CoroutineShoot());
        }
    }

    private void Reload()
    {
        if (currentAmmo < maxAmmoPerClip && currentClips > 0 && !inAction)
        {
            currentClips--;
            RefreshClipsText();
            inAction = true;
            currentAmmo = maxAmmoPerClip;
            RefreshAmmoText();
            gunAudioSource.PlayOneShot(reloadAudio);
            StartCoroutine(CoroutineReload());
        }
    }

    private void RefreshAmmoText()
    {
        ammoText.text = ammoStartingText + currentAmmo.ToString();
    }

    private void RefreshLifeText()
    {
        lifeText.text = lifeStartingText + currentLife.ToString();
    }

    private void RefreshPointsText()
    {
        scoreText.text = scoreStartingText + currentPoints.ToString();
    }

    private void RefreshClipsText()
    {
        clipsText.text = clipsStartingText + currentClips.ToString();
    }

    public void TakeDamage(int damage)
    {
        currentLife -= damage;
        RefreshLifeText();
        if(currentLife <= 0)
        {
            GameManager.Get().SetPoints(currentPoints);
            GameManager.Get().LoadEndScene();
        }
    }

    public void RecievePoints(int p)
    {
        currentPoints += p;
        RefreshPointsText();
    }

    public void AddClip()
    {
        if(currentClips < maxAmmoClips)
        {
            currentClips++;
            RefreshClipsText();
        }
    }

    IEnumerator CoroutineReload()
    {
        gunAnimator.SetTrigger(reloadAnimationTriggerName);
        yield return new WaitForSeconds(reloadSpeed);
        inAction = false;
    }

    IEnumerator CoroutineShoot()
    {
        gunAnimator.SetTrigger(shootAnimationTriggerName);
        yield return new WaitForSeconds(shootingSpeed);
        inAction = false;
    }

}

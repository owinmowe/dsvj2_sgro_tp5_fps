﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Tooltip("To check ground position for spawning")][SerializeField] Terrain terrain; 
    [Header("Bomb Related")]
    [SerializeField] GameObject bombPrefab;
    [SerializeField] float maxDistanceSpawnFromCenter = 200;
    [SerializeField] float bombCreationTime = 10;
    [SerializeField] int maxBombAmmount = 25;
    float currentBombCreationTimer = 0;
    int currentBombAmmount = 0;

    [Header("Box Related")]
    [SerializeField] GameObject boxPrefab;
    [SerializeField] float boxCreationTime = 10;
    [SerializeField] int maxBoxAmmount = 25;
    float currentBoxCreationTimer = 0;
    int currentBoxAmmount = 0;

    private void Start()
    {
        currentBombCreationTimer = bombCreationTime;
        currentBoxCreationTimer = boxCreationTime;
        for (int i = 0; i < maxBombAmmount; i++)
        {
            CreateBomb();
        }
        for (int i = 0; i < maxBoxAmmount; i++)
        {
            CreateBox();
        }
    }

    private void Update()
    {
        BombSpawnLogic();
        BoxSpawnLogic();
    }

    private void BombSpawnLogic()
    {
        currentBombCreationTimer -= Time.deltaTime;
        if (currentBombCreationTimer < 0 && currentBombAmmount < maxBombAmmount)
        {
            CreateBomb();
            currentBombCreationTimer = bombCreationTime;
        }
    }

    private void CreateBomb()
    {
        Vector3 pos = Random.insideUnitSphere;
        pos *= maxDistanceSpawnFromCenter;
        pos.x += terrain.terrainData.size.x / 2;
        pos.z += terrain.terrainData.size.z / 2;
        pos.y = terrain.SampleHeight(pos);
        var go = Instantiate(bombPrefab, pos, Quaternion.identity, transform);
        go.GetComponent<Bomb>().SetSpawner(this);
        currentBombAmmount++;
    }

    public void RemoveBomb()
    {
        currentBombAmmount--;
    }

    private void BoxSpawnLogic()
    {
        currentBoxCreationTimer -= Time.deltaTime;
        if (currentBoxCreationTimer < 0 && currentBoxAmmount < maxBoxAmmount)
        {
            CreateBox();
            currentBoxCreationTimer = boxCreationTime;
        }
    }

    private void CreateBox()
    {
        Vector3 pos = Random.insideUnitSphere;
        pos *= maxDistanceSpawnFromCenter;
        pos.x += terrain.terrainData.size.x / 2;
        pos.z += terrain.terrainData.size.z / 2;
        pos.y = terrain.SampleHeight(pos);
        var go = Instantiate(boxPrefab, pos, Quaternion.identity, transform);
        go.GetComponent<Box>().SetSpawner(this);
        currentBoxAmmount++;
    }

    public void RemoveBox()
    {
        currentBoxAmmount--;
    }

}

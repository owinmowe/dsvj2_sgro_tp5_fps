﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_EndMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI PointsText; 
    void Start()
    {
        PointsText.text += GameManager.Get().GetPoints().ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour, IDamageable
{

    [Header("Gameplay Related")]
    [SerializeField] int damage = 50;
    [SerializeField] int life = 100;
    [SerializeField] int points = 100;
    [SerializeField] float explosionRadius = 5f;
    bool dead = false;

    [Header("Animation Related")]
    [SerializeField] Animator anim;
    [SerializeField] string ExplosionTriggerName = "Explosion";
    [SerializeField] float timeForExplosion = 1f;
    [SerializeField] float timeToDisappearAfterExplosion = 2f;

    [Header("Audio Related")]
    [SerializeField] AudioClip explosionSound;
    AudioSource audioSource;

    Spawner spawner;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!dead && other.gameObject.layer == GameManager.Get().getCurrentPlayerReference().gameObject.layer)
        {
            StartCoroutine(CoroutineExplosion());
        }
    }

    public void TakeDamage(int damage)
    {
        life -= damage;
        if(!dead && life <= 0)
        {
            StartCoroutine(CoroutineExplosion());
            GameManager.Get().getCurrentPlayerReference().RecievePoints(points);
        }
    }

    public void SetSpawner(Spawner newSpawner)
    {
        spawner = newSpawner;
    }

    IEnumerator CoroutineExplosion()
    {
        dead = true;
        anim.SetTrigger(ExplosionTriggerName);
        yield return new WaitForSeconds(timeForExplosion);
        audioSource.PlayOneShot(explosionSound);
        spawner.RemoveBomb();
        Destroy(gameObject, timeToDisappearAfterExplosion);
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, explosionRadius, transform.forward);
        foreach (var hit in hits)
        {
            IDamageable testInterface = hit.collider.gameObject.GetComponent<IDamageable>();
            if (testInterface != null)
            {
                testInterface.TakeDamage(damage);
            }
        }
    }

}

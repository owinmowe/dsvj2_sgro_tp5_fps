﻿using UnityEngine;

public class Box : MonoBehaviour
{
    [Header("Gameplay Related")]
    [SerializeField] int playerLayer = 9;
    [SerializeField] int points = 100;

    Spawner spawner;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == playerLayer)
        {
            other.gameObject.GetComponent<Player>().RecievePoints(points);
            other.gameObject.GetComponent<Player>().AddClip();
            spawner.RemoveBox();
            Destroy(gameObject);
        }
    }

    public void SetSpawner(Spawner newSpawner)
    {
        spawner = newSpawner;
    }
}
